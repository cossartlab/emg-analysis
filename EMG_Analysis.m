%%%%%%%% Script for analyzing EMG data from neonates. 

%%%%%%%%
%%%%%%%%
% Part 1: generate variables. 

% Perform 300 hz high pass filter of EMG data and save for record.
Sampling_Rate=1000; % Sampling rate of EMG data (downsampled from 16000 Hz).
EMG_Data_Raw=importdata('CSC001.mat'); % Import downsampled EMG data.
flter1=fir1(Sampling_Rate,[300/(Sampling_Rate/2)],'high'); % Design filter.
EMG_Data_Filtered_Rectified=filtfilt(flter1,1,EMG_Data_Raw); % Filter the EMG data.
EMG_Data_Filtered_Rectified=abs(EMG_Data_Filtered_Rectified); % Rectify the EMG data.
save('EMG_Data_Filtered_Rectified.mat','EMG_Data_Filtered_Rectified'); % Save EMG data.

% Indicate 5 timepoints (in ms from start of recording) to use to calculate
% mean amplitude of EMG data during periods with relatively high muscle
% tone. Then do the same for periods with relatively low muscle tone.
High_Muscle_Tone_Timepoints_Pre=importdata('High_Muscle_Tone_Timepoints.mat'); % Format; single column with rows that are timepoints (in ms) of start of 1000 ms period to use for generation of mean tonic periods. 
High_Muscle_Tone_Timepoints=zeros(length(High_Muscle_Tone_Timepoints_Pre(:,1)),2);
High_Muscle_Tone_Timepoints(:,1)=High_Muscle_Tone_Timepoints_Pre;
High_Muscle_Tone_Timepoints(:,2)=High_Muscle_Tone_Timepoints(:,1)-1+1000;

Low_Muscle_Tone_Timepoints_Pre=importdata('Low_Muscle_Tone_Timepoints.mat'); % Format; single column with rows that are timepoints (in ms) of start of 1000 ms period to use for generation of mean atonic periods.
Low_Muscle_Tone_Timepoints=zeros(length(Low_Muscle_Tone_Timepoints_Pre(:,1)),2);
Low_Muscle_Tone_Timepoints(:,1)=Low_Muscle_Tone_Timepoints_Pre;
Low_Muscle_Tone_Timepoints(:,2)=Low_Muscle_Tone_Timepoints(:,1)-1+1000;

% Determine mean amplitude of rectified filtered EMG signal during periods
% with muscle tone and atonia. Use these values to determine midpoint
% between tonic and atonic periods of muscle tone. High activity periods
% will be used to further differentiate sleep into active vs. inactive.
High_Muscle_Data_Points=zeros(length(High_Muscle_Tone_Timepoints(:,1))*Sampling_Rate,1);
Low_Muscle_Data_Points=zeros(length(Low_Muscle_Tone_Timepoints(:,1))*Sampling_Rate,1);
for x=1:length(High_Muscle_Tone_Timepoints(:,1))
    High_Muscle_Data_Points(1+(x-1)*Sampling_Rate:Sampling_Rate+(x-1)*Sampling_Rate,1)=EMG_Data_Filtered_Rectified(High_Muscle_Tone_Timepoints(x,1):High_Muscle_Tone_Timepoints(x,2),1);
    Low_Muscle_Data_Points(1+(x-1)*Sampling_Rate:Sampling_Rate+(x-1)*Sampling_Rate,1)=EMG_Data_Filtered_Rectified(Low_Muscle_Tone_Timepoints(x,1):Low_Muscle_Tone_Timepoints(x,2),1);
end
Mean_High_Tone=mean(High_Muscle_Data_Points(:,1));
Std_High_Tone=std(High_Muscle_Data_Points(:,1));
Mean_Low_Tone=mean(Low_Muscle_Data_Points(:,1));
Std_Low_Tone=std(Low_Muscle_Data_Points(:,1));
High_Low_Tone_Midpoint_Threshold=(Mean_High_Tone+Mean_Low_Tone)/2; % This value will be used to determine sleep from wakefulness.
High_Low_Tone_Quarter_Point_Threshold=(High_Low_Tone_Midpoint_Threshold+Mean_Low_Tone)/2; % This value will be used to determine periods of transitional sleep between REMs and wakefulness.

save('Mean_High_Tone.mat','Mean_High_Tone');
save('Std_High_Tone.mat','Std_High_Tone');
save('Mean_Low_Tone.mat','Mean_Low_Tone');
save('Std_Low_Tone.mat','Std_Low_Tone');
save('High_Low_Tone_Midpoint_Threshold.mat','High_Low_Tone_Midpoint_Threshold');
save('High_Low_Tone_Quarter_Point_Threshold.mat','High_Low_Tone_Quarter_Point_Threshold');
clear

%%%%%%%%
%%%%%%%%
% Part 2: generate hypnogram. 

clear
Record_Start_ms=importdata('Record_Start_ms.mat'); % Camera acquisition start time; used to synchronize camera and EMG recordings.
Record_End_ms=importdata('Record_End_ms.mat'); % Camera acquisition end time; used to synchronize camera and EMG recordings.

EMG_Data_Filtered_Rectified=importdata('EMG_Data_Filtered_Rectified.mat');

Mean_High_Tone=importdata('Mean_High_Tone.mat');
Std_High_Tone=importdata('Std_High_Tone.mat');
Mean_Low_Tone=importdata('Mean_Low_Tone.mat');
Std_Low_Tone=importdata('Std_Low_Tone.mat');
High_Low_Tone_Midpoint_Threshold=importdata('High_Low_Tone_Midpoint_Threshold.mat');
High_Low_Tone_Quarter_Point_Threshold=importdata('High_Low_Tone_Quarter_Point_Threshold.mat');

Sampling_Rate=1000;
Epoch_Size_s=1;

% Next step is to automatically determine sleep vs wake epochs.
Number_of_Epochs=floor((length(EMG_Data_Filtered_Rectified(:,1))/Sampling_Rate)/Epoch_Size_s);

Hypnogram=zeros(length(EMG_Data_Filtered_Rectified(:,1)),1);

for x=1:Number_of_Epochs
    Mean_Muscle_Tone_Current_Epoch=mean(EMG_Data_Filtered_Rectified(1+(x-1)*Sampling_Rate*Epoch_Size_s:Sampling_Rate*Epoch_Size_s+(x-1)*Sampling_Rate*Epoch_Size_s,1));
    if Mean_Muscle_Tone_Current_Epoch>=High_Low_Tone_Midpoint_Threshold % If high muscle tone.
        Hypnogram(1+(x-1)*Sampling_Rate*Epoch_Size_s:Sampling_Rate*Epoch_Size_s+(x-1)*Sampling_Rate*Epoch_Size_s,1)=1; % Label as wake.
    elseif Mean_Muscle_Tone_Current_Epoch<High_Low_Tone_Midpoint_Threshold && Mean_Muscle_Tone_Current_Epoch>=High_Low_Tone_Quarter_Point_Threshold % If moderately low muscle tone.
        Hypnogram(1+(x-1)*Sampling_Rate*Epoch_Size_s:Sampling_Rate*Epoch_Size_s+(x-1)*Sampling_Rate*Epoch_Size_s,1)=2; % Label as transition sleep.
    elseif Mean_Muscle_Tone_Current_Epoch<High_Low_Tone_Midpoint_Threshold && Mean_Muscle_Tone_Current_Epoch<High_Low_Tone_Quarter_Point_Threshold % If low muscle tone, is REMs.
        % Check for presence of muscle twitches within REMs period to differentiate between active and non-active REMs periods.
        for xx=1:(Sampling_Rate*Epoch_Size_s) % For each time point within current epoch.
            if EMG_Data_Filtered_Rectified(1+(x-1)*Sampling_Rate*Epoch_Size_s+(xx-1),1)<=(Mean_Low_Tone+5*Std_Low_Tone) % If significant twitch is not present.
                EMG_Data_Filtered_Rectified(1+(x-1)*Sampling_Rate*Epoch_Size_s+(xx-1),1)=3; % Label as non-active REMs.
            elseif EMG_Data_Filtered_Rectified(1+(x-1)*Sampling_Rate*Epoch_Size_s+(xx-1),1)>(Mean_Low_Tone+5*Std_Low_Tone) % If significant twitch is present.
                EMG_Data_Filtered_Rectified(1+(x-1)*Sampling_Rate*Epoch_Size_s+(xx-1),1)=4; % Label as active REMs.
            end
        end
    end
end

% Indicate boundaries of camera recording within hypnogram file (used to
% synchronize camera and EMG data).
Hypnogram(1:Record_Start_ms,1)=nan;
Hypnogram(Record_End_ms:end,1)=nan;

% Save for records.
save('Hypnogram.mat','Hypnogram'); 





